
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import { LibDiamond } from "../libraries/LibDiamond.sol";
import { AssetListingHandler } from "../libraries/LibAssetListing.sol";

contract AddToListing {

     function addTolisting(uint256 assetId, uint256 copies,uint256 premium, bytes memory data) external  {
        //Do not allow if the assetId already been used
        // require(totalSupply(assetId) == 0, "asset already been listed");
        //set the premium price
        // AssetPriceHandler._setPremiumPrice(assetId, premium);
        //Mint the token without any offchain process and assign it to creator/owner of the project
        // _mint(msg.sender, assetId, copies, data);
        //make sure it is added to the listing as well, so that it can be purchased
        AssetListingHandler._listOwnerAssets(msg.sender, assetId, copies, copies);
    }

    function test(uint256 id) external {
        
    }
}