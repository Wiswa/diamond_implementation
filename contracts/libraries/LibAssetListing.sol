// SPDX-License-Identifier: MIT    
pragma solidity ^0.8.0;   

// import "@openzeppelin/contracts/access/AccessControl.sol";
// import "hardhat/console.sol";
import {AppStorage, LibAppStorage} from "../libraries/LibAppStorage.sol";
import {LibDiamond} from "../libraries/LibDiamond.sol";
//import "../util/ArrayUtils.sol";


/// @title Token listing by owner to sell in the market
/// @author Malla Reddy
/// @notice Asset with count can be listed for sale in the market
library AssetListingHandler {
  
    /// @notice Adds a new NFT with number of copies.
    /// @dev After listing the NFT with copies, purchasing/minting is allowed for any user.
    /// @param assetId Identifier of the NFT for which copies to be listed.
    /// @param copies Number of copies to be listed for a NFT.

    event AssetListed(address assetOwner, uint assetId, uint copies, uint totalCopies);

    function _listOwnerAssets(address assetOwner, uint assetId, uint copies, uint totalCopiesOwned) internal {
        //available for listing cannot be more than ==> totalCopiesOwned -  copiesHeld
        AppStorage storage s = LibAppStorage.diamondStorage();
        require(copies <= totalCopiesOwned - s._assetInProcess[assetId][assetOwner], "cannot list more than listable count");
        s._assetListings[assetId][assetOwner] = copies;
        s._assetListedOwners[assetId].push(assetOwner);
        emit AssetListed(assetOwner, assetId ,copies, totalCopiesOwned);
    }

    function _listingToProcess(address from, address to, uint assetId,uint copies, uint processId) internal {
        AppStorage storage s = LibAppStorage.diamondStorage();
        require(s._assetListings[assetId][from] >= copies,"listing does not have enough count");
        s._assetListings[assetId][from] -= copies;
        s._assetInProcess[assetId][to] += copies;
        s._assetHeldOwners[assetId].push(to);
        s._pendingProcIds[assetId][to].push(processId);
        //ArrayUtils.removeElement(_assetListedOwners[assetId], from);
    }

    function _completInProcess(address from, address to, uint assetId,uint copies) internal{
        AppStorage storage s = LibAppStorage.diamondStorage();
        require(s._assetInProcess[assetId][to] >= copies,"no such asset held");
        s._assetInProcess[assetId][to] -= copies;
        s._assetListedOwners[assetId].push(to);
        s._assetListings[assetId][to] += copies;
    }

    /// @notice Get the NFT count available for minting
    /// @dev Get the NFT copies that are available for purchase/mint
    /// @param assetId NFT reference id, should be unique in a project
    /// @return NFT copies avaialble count
    // function getAssetListing(address assetOwner, uint assetId ) external view returns(uint) {
    //     return _getAssetListing(assetOwner, assetId);
    // }

    function _getAssetListing(address assetOwner,uint assetId ) internal view returns(uint) {
        AppStorage storage s = LibAppStorage.diamondStorage();
        return s._assetListings[assetId][assetOwner];
    }

    event AssetHeld(address assetOwner, uint assetId, uint copies, uint totalCopies);

    function _holdOwnerAssets(address assetOwner, uint assetId,uint copies, uint totalCopiesOwned) internal {
        AppStorage storage s = LibAppStorage.diamondStorage();
        s._assetInProcess[assetId][assetOwner] = copies;
        s._assetHeldOwners[assetId].push(assetOwner);
        //ArrayUtils.removeElement(_assetListedOwners[assetId], assetOwner);
        emit AssetHeld( assetOwner,  assetId,  copies, totalCopiesOwned);
    }

    function getAssetHeld(address assetOwner, uint assetId)  internal view returns(uint, uint[] memory ) {
        return _getAssetHeld(assetOwner, assetId);
    }

    function _getAssetHeld(address assetOwner,uint assetId ) internal view returns(uint, uint[] memory) {
        AppStorage storage s = LibAppStorage.diamondStorage();
        return (s._assetInProcess[assetId][assetOwner], s._pendingProcIds[assetId][assetOwner] );

    }

    function getAssetListedOwners(uint assetId ) internal view returns(address[] memory) {
        AppStorage storage s = LibAppStorage.diamondStorage();
        return s._assetListedOwners[assetId];
    }

    function getAssetHeldOwners(uint assetId ) internal view returns(address[] memory) {
        AppStorage storage s = LibAppStorage.diamondStorage();
        return s._assetHeldOwners[assetId];
    }
}