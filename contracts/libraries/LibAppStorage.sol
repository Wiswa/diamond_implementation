pragma solidity 0.8.0;

import {LibDiamond} from "../libraries/LibDiamond.sol";

// struct Aavegotchi {
//     address owner;
// }

struct AppStorage{
     
     //events 
    //Asset owned by a user has been listed(which is not held)
    // event AssetListed(address assetOwner, uint assetId, uint copies, uint totalCopies);
    //Asset owned by a user has been held for some reason. An asset cannot be held and listed at the same time.
    // event AssetHeld(address assetOwner, uint assetId, uint copies, uint totalCopies);

    //Assets that are listed for the sale by owner
    mapping(uint256 => mapping(address => uint256))  _assetListings;

    //Assets that are not allowed to be listed as the assets are in approval process or on hold for some reason
    mapping(uint256 => mapping(address => uint256))  _assetInProcess;

    // mapping(uint256 => mapping(address => uint256[])) private _assetInProcess_;
    //holds the asset listing owners mapping to assetId
    mapping(uint256 => address[])  _assetListedOwners;

    //asset held owners mapping to assetId
    mapping(uint256 => address[])  _assetHeldOwners;

    //Process Id mapping
    mapping(uint256 => mapping(address => uint256[])) _pendingProcIds;


}

library LibAppStorage {
    function diamondStorage() internal pure returns (AppStorage storage ds) {
        assembly {
            ds.slot := 0
        }
    }
}
