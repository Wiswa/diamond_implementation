const path = require("path");

require("dotenv").config();
require("@nomiclabs/hardhat-etherscan");
require("@nomiclabs/hardhat-waffle");
require("hardhat-gas-reporter");
require("solidity-coverage");
require("shelljs");
require('solidity-docgen');



/**
 * Extending the hardhat cofig to compile the Contracts from Customer Sources path
 * and keep the artifacts in Customer Path. Which will enable the system to run 
 * hardhat compilation multiple times and parallelly without impacting/conflicting
 * the ongoing hardahat execution.
 */

 extendConfig(
  (config, userConfig) => {
    console.log('extending the HardHatConfig');
    
    /*
     * There should be a better way of hanlding the arguments. we should try later wherther 
     * global envirobnment can be used.
     * For now, hack way to find the artifacts folder, src folder via process.argsv.
     */
    const index = process.argv.indexOf('--artifacts');
    
    if(index > 0) {
      const artifactsPath = process.argv[index + 1];
      console.log('Custom artifacts path is', artifactsPath);
      config.paths.artifacts = path.join(config.paths.root, artifactsPath);
    }

    const srcIndex = process.argv.indexOf('--sources');
    
    if(index > 0) {
      const srcPath = process.argv[srcIndex + 1];
      console.log('Custom sources path is', srcPath);
      config.paths.sources = path.join(config.paths.root, srcPath);
    }
  });

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});


task("custom-compile", 
    "Prints the list of accounts")
    .addParam("sources", "holds the custom sources directory of contracts")
    .addParam("artifacts", "holds the custom artifacts directory")
    .setAction(async (taskArgs, hre) => {
        console.log('Doing custome compile');
        console.log('Source dir is ', hre.config.paths.sources);
        console.log('artifacts dir is ', hre.config.paths.artifacts)
        await hre.run('compile', {'force': true});
});


task(
  "blockNumber",
  "Prints the current block number",
  async (_, { ethers }) => {
    await ethers.provider.getBlockNumber().then((blockNumber) => {
      console.log("Current block number: " + blockNumber);
    });
  }
);



task("balance", "Prints an account's balance")
  .addParam("account", "The account's address")
  .setAction(async (taskArgs) => {
    const account = web3.utils.toChecksumAddress(taskArgs.account);
    const balance = await web3.eth.getBalance(account);

    console.log(web3.utils.fromWei(balance, "ether"), "ETH");
  });

task("deployToNetwork", "Deploy smart contract to local")
  .addParam("projDetails","Project Details")
  .setAction(async (taskArgs) => {

  var projDetails = JSON.parse(taskArgs.projDetails);
  
  //Project params
  const _projId = projDetails._projId;
  const _nftId = projDetails._nftId;
  const _mingoal = projDetails._mingoal;
  const _maxgoal = projDetails._maxgoal;
  const _durationInDays = projDetails._durationInDays;
  const _assetPrice = projDetails._assetPrice;
  const _initialOfferPrice = projDetails._initialOfferPrice;
  
  console.log("Deploying projDetails: ",projDetails);
  console.log("Deploying projId: ",_projId);
  
  var _deployStartTime = Math.floor(new Date().getTime()/1000) - 10 ;
  var _startAt = 0; //now

  //var operator = process.env.OPERATOR_LOCAL_PRIVATE_KEY;
  //var operator = ethers.getSigner('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266');
  var operator_address = '0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266';

  // We get the contract to deploy
  const RealToken = await ethers.getContractFactory("RealToken");
  //creator deploying the contract
  var RealTokenInstance = await RealToken.deploy(_projId, _assetPrice, _initialOfferPrice, operator_address, operator_address, operator_address);
  var _deployEndTime = Math.floor(new Date().getTime()/1000);


  console.log("RealToken deployed to:", RealTokenInstance.address);

  console.timeEnd("Total test time");
});


// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: {
    version: "0.8.0",
    settings: {
      optimizer: {
        enabled: true,
        runs: 1000,
      },
    },
  },
  
  networks: {
    hardhat: {
      allowUnlimitedContractSize : true, //this could be an issue when deploying to mainnet - https://github.com/dethcrypto/TypeChain/issues/375
      initialBaseFeePerGas: 0, // workaround from https://github.com/sc-forks/solidity-coverage/issues/652#issuecomment-896330136 . Remove when that issue is closed.
    },
    
    //networks: 
    //https://github.com/NomicFoundation/hardhat/blob/cbfaa49924e399f13d006f350c5e1fe055631063/packages/hardhat-etherscan/src/types.ts
    mainnet: {
      url: process.env.MAINNET_URL || "",
      accounts:
        process.env.PRIVATE_KEY !== undefined ? [process.env.PRIVATE_KEY] : [],
    },
    ropsten: {
      url: process.env.ROPSTEN_URL || "",
      accounts:
        process.env.PRIVATE_KEY !== undefined ? [process.env.PRIVATE_KEY] : [],
    },
    matic: {
      url: process.env.POL_MAINNNET_URL || "",
      accounts:
        process.env.PRIVATE_KEY !== undefined ? [process.env.PRIVATE_KEY] : [],
    },
    maticMumbai: {
      url: process.env.POL_MUMBAI_URL || "",
      accounts:
        process.env.PRIVATE_KEY !== undefined ? [process.env.PRIVATE_KEY] : [],
    },
    rinkeby: {
      url: process.env.RINKEBY_URL || "",
      accounts:
        process.env.PRIVATE_KEY !== undefined ? [process.env.PRIVATE_KEY] : [],
    }
  },
  gasReporter: {
    //refer - https://www.npmjs.com/package/hardhat-gas-reporter
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
    coinmarketcap: process.env.COIN_MARKET_CAP_API_KEY,
    token: 'AVAX'
  },
  etherscan: {
    apiKey: process.env.ETHERSCAN_API_KEY,
  },
  docgen:{
    pages: 'files'
  }
};
